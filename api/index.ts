export const config = {
  runtime: 'edge'
};

const handler = async (request: Request): Promise<Response> => {
  const lookupTableUrl = 'https://djf45trtmszc1.cloudfront.net/';
  const reqHost = request.headers.get('host').split(':')[0].split('.').join('_'); //Remove the port from the host header, replace dots with underscores
  const reqPath = request.url.split('/').splice(3).join('/');
  const targetHost: string = (await fetch(lookupTableUrl+reqHost+'.json').then(response => response.json()).catch(error => null)) || (await fetch(lookupTableUrl+'default.json').then(response => response.json()));
  const targetUrl = reqPath ? 'https://'+targetHost+'/'+reqPath : 'https://'+targetHost;
  const landingPageHost = targetUrl.split('/')[2];
  const newHeaders = {...request.headers, 'host': landingPageHost};

  const response = await fetch(targetUrl,
    {
      method: request.method,
      headers: newHeaders,
    });
  return response;

};

export default handler;
